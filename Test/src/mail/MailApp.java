package mail;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
/**
 * Servlet implementation class MailApp
 */
@WebServlet(name = "mail", urlPatterns = { "/mail" })
public class MailApp extends HttpServlet {
	   protected void doPost(HttpServletRequest request, HttpServletResponse response)
	            throws ServletException, IOException {
		    response.setContentType("text/html");  
		    PrintWriter out = response.getWriter();  
		      
		    String to=request.getParameter("to");  
		    String subject=request.getParameter("subject");  
		    String msg=request.getParameter("msg");  
		     SendMail send=new SendMail();     
		    try {
				send.test(to, subject, msg);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
		    out.print("message has been sent successfully");  
		    out.close();  

}}
